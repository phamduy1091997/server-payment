package com.phuongbanker.demo.service;

import com.phuongbanker.demo.entity.dao.PhuongBanker;
import org.springframework.stereotype.Service;

@Service
public interface ProcessPayment {
	PhuongBanker withdraw(String customerId, String dataPayment);
	void transfer(String customerId,String dataPayment);
	void deposit(String customerId,String dataPayment);
}
