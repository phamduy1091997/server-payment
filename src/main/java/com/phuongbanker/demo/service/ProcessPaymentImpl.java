package com.phuongbanker.demo.service;

import com.phuongbanker.demo.entity.OrderPayments;
import com.phuongbanker.demo.entity.dao.Item;
import com.phuongbanker.demo.entity.dao.PhuongBanker;
import com.phuongbanker.demo.handleException.InsufficientBalanceException;
import com.phuongbanker.demo.handleException.UserNotFoundException;
import com.phuongbanker.demo.repo.ItemRepository;
import com.phuongbanker.demo.repo.PhuongBankerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ProcessPaymentImpl implements ProcessPayment {
	@Autowired
	ItemRepository itemRepository;

	@Autowired
	PhuongBankerRepository  phuongBankerRepository;


	@Override
	public PhuongBanker withdraw(String customerId, String dataPayment) {
		Item item = itemRepository.findByGoodId(dataPayment);
		PhuongBanker customer = phuongBankerRepository.findByCustomerId(customerId);
		if (customer == null) {
			throw new UserNotFoundException("0");
		}
		throwIfNotEnoughMoney(item, customer);
	 	customer.setTotalAmount(customer.getTotalAmount() - item.getPrice());
	 	return  customer;
	}
	private void throwIfNotEnoughMoney(Item item, PhuongBanker customerId) {
		if (customerId.getTotalAmount() - item.getPrice() < 0) {
			throw new InsufficientBalanceException((int) customerId.getTotalAmount(), "1");
		}
	}

	@Override
	public void transfer(String customerId, String dataPayment) {

	}


	@Override
	public void deposit(String customerId, String dataPayment) {

	}
}
