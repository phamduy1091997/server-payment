package com.phuongbanker.demo.service;

import com.phuongbanker.demo.entity.dao.PhuongBanker;
import com.phuongbanker.demo.repo.CustomerAuthRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import ultil.CryptoUtil;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class HashService {

    @Autowired
    CustomerAuthRepository customerAuthRepository;

    @Autowired
    PhuongBanker phuongBanker;

    @Autowired
    ProcessPaymentImpl processPaymentImpl;

    public Map<String, String> decrypt(String customerId, String encryptData, String encryptKey) throws Exception {
        Map<String, String> result = new HashMap<>();

        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(StandardCharsets.UTF_8));

        //Load privateKey and publicKey in database
        String partnerPrivateKey = (customerAuthRepository.findByCustomerId(customerId)).getPartnerPrivateKey();
        String myCompanyPublicKey = (customerAuthRepository.findByCustomerId(customerId)).getMyCompanyPublicKey();
        //Decrypt Datagetg
        byte[] decryptKey = CryptoUtil.decryptRSAToByte(encryptKey, partnerPrivateKey);

        String data = CryptoUtil.decryptTripleDes(encryptData, decryptKey);

        //Check signature
        String checkData = data.substring(0, data.lastIndexOf("|"));

        String signature = data.substring(data.lastIndexOf("|") + 1);

        boolean verify = CryptoUtil.verifyRSA(checkData, signature, myCompanyPublicKey);

        if (verify) {
            String accessCode = checkData.substring(0, checkData.indexOf("|"));
            String dataPayments = checkData.substring(2);
            switch (accessCode) {
                case "1":
                    System.out.println("Payment for fun");
                    processPaymentImpl.withdraw(customerId, dataPayments);
                case "transfer":
                    System.out.println("Transfer for fun");
                    processPaymentImpl.transfer(customerId, dataPayments);
                case "deposit":
                    System.out.println("Deposit for fun");
                    processPaymentImpl.deposit(customerId, dataPayments);
            }
            result.put("checkData", checkData);
            result.put("resultCode", "1");
            result.put("resultStatus", "Success");

            return HashService.encrypt(result, partnerPrivateKey, myCompanyPublicKey);
        } else {
            result.put("resultStatuss", "999");
            result.put("statusSignature", "Signature is invalid");
            return result;
        }
    }

    public static Map<String, String> encrypt(Map<String, String> rawData, String privateKey, String publicKey) throws Exception {
        String signature = CryptoUtil.signRSA(String.valueOf(rawData), privateKey);

        String data = rawData + "|" + signature;

        byte[] byteKey = CryptoUtil.generateKey();

        String encryptData = CryptoUtil.encryptTripleDes(data, byteKey);

        String encryptKey = CryptoUtil.encryptRSA(byteKey, publicKey);

        Map<String, String> response = new HashMap<>();
        response.put("encryptData", encryptData);
        response.put("encryptKey", encryptKey);

        return response;
    }

}
