package com.phuongbanker.demo.handleException;

public class OrderPaymentNotValid extends RuntimeException{
    public OrderPaymentNotValid(final String message){
        super(message);
    };
}
