package com.phuongbanker.demo.handleException;

import lombok.Getter;

public class InsufficientBalanceException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	@Getter
	private final long amount;

	public InsufficientBalanceException(final int amount, final String message) {
		super(message);
		this.amount = amount;
	}
}
