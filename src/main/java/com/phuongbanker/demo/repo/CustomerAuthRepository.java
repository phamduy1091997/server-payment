package com.phuongbanker.demo.repo;

import com.phuongbanker.demo.entity.dao.CustomerAuth;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;

@Component
public interface CustomerAuthRepository extends MongoRepository<CustomerAuth,String> {

	CustomerAuth findByCustomerId(String customerId);

}
