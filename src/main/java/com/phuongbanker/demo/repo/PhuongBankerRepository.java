package com.phuongbanker.demo.repo;

import com.phuongbanker.demo.entity.dao.PhuongBanker;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface PhuongBankerRepository extends MongoRepository<PhuongBanker,String> {
	PhuongBanker findByCustomerId(String customerId);
}
