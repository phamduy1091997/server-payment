package com.phuongbanker.demo.repo;

import com.phuongbanker.demo.entity.dao.Item;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ItemRepository extends MongoRepository<Item,String> {
	Item findByGoodId(String goodId);
}
