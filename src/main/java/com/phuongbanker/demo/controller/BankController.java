package com.phuongbanker.demo.controller;

import com.phuongbanker.demo.entity.DecryptRequest;
import com.phuongbanker.demo.service.HashService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class BankController {

	@Autowired
	private HashService hashService;


	@PostMapping("/decrypt")
	public ResponseEntity<Map<String,String>> decrypt (@RequestBody DecryptRequest decryptRequest) throws Exception {


		Map<String,String> response = new HashMap();

		System.out.println(response + "response");

		try {
			System.out.println(decryptRequest.getCustomerId());
			System.out.println(decryptRequest.getEncryptData());
			System.out.println(decryptRequest.getEncryptKey());

			response = hashService.decrypt(decryptRequest.getCustomerId(), decryptRequest.getEncryptData(), decryptRequest.getEncryptKey());
		} catch (Exception e) {
			response.put("status","Error");
			e.printStackTrace();
			return ResponseEntity.badRequest().body(response);

		}

		return ResponseEntity.ok(response);
	}

}
