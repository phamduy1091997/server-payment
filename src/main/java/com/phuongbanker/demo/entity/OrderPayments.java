package com.phuongbanker.demo.entity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OrderPayments {
	private String id;
	private String customerId;
	private long price;
}
