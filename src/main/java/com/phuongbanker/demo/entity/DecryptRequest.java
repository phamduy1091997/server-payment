package com.phuongbanker.demo.entity;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DecryptRequest {

	private String customerId;
	private String encryptData;
	private String encryptKey;

}
