package com.phuongbanker.demo.entity.dao;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.stereotype.Component;

@Getter
@Setter
@Component
@Document(collection = "item")
public class Item {

	@Id
	private String id;
	private String goodId;
	private long price;

}
