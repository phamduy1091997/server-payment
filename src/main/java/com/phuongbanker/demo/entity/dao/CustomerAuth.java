package com.phuongbanker.demo.entity.dao;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.stereotype.Component;

import java.io.Serializable;

@Getter
@Setter
@Component
@Document(collection = "customer_auth")
public class CustomerAuth implements Serializable {
	@Id
	private String id;
	private String customerId;
	private String partnerPrivateKey;
	private String myCompanyPublicKey;
}
